Role: gitlab-runner-install
=========
A brief description of the role goes :

This role install gitlab-runners service on your host.

Requirements :pushpin:
------------
Pre-requisites that may not be covered by Ansible : 

- **python3** (master node and managed node)

Role Variables :wrench:
--------------

```yml
#default/main.yml
gitlab_runner_repo: https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh
download_script_path: /root/gitlab-runner-repository.sh

```

Dependencies :paperclip:
------------
A list of other roles should go here and any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles :

None

Example Playbook :clapper:
----------------

```yml 
- name : Playbook > install service gitlab-runners 
  gather_facts: true
  become: true
  hosts:
    - all
  roles:
    - gitlab-runner-install
```
